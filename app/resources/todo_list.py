from flask_restful import Resource, reqparse
from models.todo_item import TodoItemModel, UserModel
from flask_jwt_extended import jwt_required, get_jwt_identity

from security import non_empty_string

def item_status_string(s):
    if isinstance(s, str):
        if len(s) != 1:
            raise ValueError("String is longer than one character.")
        else:
            s = s.upper()
            if s not in TodoItemModel.ITEM_STATUS:
                raise ValueError('Invalid status code.')
            return s
    raise ValueError('Single-character string is expected.')



upsert_item_parser = reqparse.RequestParser()
upsert_item_parser.add_argument('subject', type=non_empty_string, required=True, help='subject cannot be blank.')
upsert_item_parser.add_argument('detail', type=str, required=False)
upsert_item_parser.add_argument('status', type=item_status_string, required=True, help='status can be either P or D')

get_item_parser = reqparse.RequestParser()
get_item_parser.add_argument('id', type=int, required=True, help='id cannot be blank.')

class TodoItemList(Resource):
    @jwt_required
    def get(self):
        return {'items': [i.json() for i in TodoItemModel.find_by_username(get_jwt_identity())]}


class TodoItemInserter(Resource):
    @jwt_required
    def post(self):
        user = UserModel.find_by_username(get_jwt_identity())
        data = upsert_item_parser.parse_args()

        item = TodoItemModel(user.id, data['subject'], data['detail'], data['status'])
        try:
            item.save_to_db()
        except:
            return {'message': 'An error occurred while saving this item.'}, 500
        return item.json(), 201

class TodoItemAccessor(Resource):
    @jwt_required
    def get(self, item_id):
        user = UserModel.find_by_username(get_jwt_identity())
        item = TodoItemModel.query.filter_by(id=item_id, user_id=user.id).first()
        if item:
            return {'item': item.json()}
        return {'message': 'Invalid item ID.'}, 400

    def upsert_item(self, item_id):
        user = UserModel.find_by_username(get_jwt_identity())
        data = upsert_item_parser.parse_args()

        item = TodoItemModel.find(user.id, item_id)

        if item:
            item.subject = data['subject']
            item.detail = data['detail']
            item.status = data['status']
            try:
                item.save_to_db()
            except:
                return {'message': f'An error occurred when updating item ID {item_id}'}, 500

            return item.json()

        return {'message': f'Cannot find item with ID {item_id}'}, 400

    @jwt_required
    def post(self, item_id):
        return self.upsert_item(item_id)

    @jwt_required
    def put(self, item_id):
        return self.upsert_item(item_id)

    @jwt_required
    def delete(self, item_id):
        user = UserModel.find_by_username(get_jwt_identity())
        item = TodoItemModel.find(user.id, item_id)

        if not item:
            return {'message': 'Invalid item ID.'}, 400

        try:
            item.delete_from_db()
        except:
            return {'message': f'An error occurred when deleting item ID {item_id}'}, 500

        return {'message': 'Item deleted.'}
