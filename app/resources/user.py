from flask_restful import Resource, reqparse
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required,
                                jwt_refresh_token_required, get_raw_jwt, get_jwt_identity)
from security import blacklist, non_empty_string
from models.user import UserModel

auth_parser = reqparse.RequestParser()
auth_parser.add_argument('username', type=non_empty_string, required=True, help='Username must not be blank.')
auth_parser.add_argument('password', type=non_empty_string, required=True, help='Password must not be blank.')

class UserRegistration(Resource):
    def post(self):
        data = auth_parser.parse_args()

        username = data['username']
        password = data['password']

        if UserModel.find_by_username(username) :
            return {'message': f'User "{username}" already exists.'}, 400

        new_user = UserModel(username, password)
        new_user.save_to_db()

        access_token = create_access_token(identity=username)
        refresh_token = create_refresh_token(identity=username)

        return {'message': f'User "{username}" has been created successfully.',
                'access_token': access_token,
                'refresh_token': refresh_token}, 201

class UserLogin(Resource):
    def post(self):
        data = auth_parser.parse_args()
        username = data['username']
        password = data['password']

        user = UserModel.find_by_username(username)

        if not user:
            return {'message': f'Wrong credentials.'}, 400

        if UserModel.verify_hash(password, user.password):
            access_token = create_access_token(identity=username)
            refresh_token = create_refresh_token(identity=username)
            return {'message': f'Logged in as "{username}"',
                    'access_token': access_token,
                    'refresh_token': refresh_token}

        return {'message': 'Wrong credentials'}, 400

class UserLogoutAccess(Resource):
    @jwt_required
    def delete(self):
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return {'message': 'Successfully logged out'}

class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def delete(self):
        jti = get_raw_jwt()['jti']
        blacklist.add(jti)
        return {'message': 'Successfully logged out'}

class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}
