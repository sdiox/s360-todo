import os
from app import create_app

config_name = os.getenv('APP_SETTINGS') or 'production'
flask_objects = create_app(config_name)
app = flask_objects['app']

if __name__ == '__main__':
    app.run(host='0.0.0.0')
