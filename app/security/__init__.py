from flask import jsonify

SECRET_KEY = 'HELLO WORLD!'

blacklist = set()
def set_jwt_loaders(jwt):
    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        jti = decrypted_token['jti']
        return jti in blacklist

    @jwt.unauthorized_loader
    def unauthorized_response(callback=None):
        return jsonify({
            'message': 'Missing Authorization Header'
        }), 401

    @jwt.expired_token_loader
    def expired_token_response(callback=None):
        return jsonify({
            'message': 'Token has expired'
        }), 401

    @jwt.revoked_token_loader
    def revoked_token_response(callback=None):
        return jsonify({
            'message': 'Token has been revoked'
        }), 401

    @jwt.invalid_token_loader
    def invalid_token_response(callback=None):
        return jsonify({
            'message': 'Invalid token'
        }), 401

def non_empty_string(s):
    if isinstance(s, str):
        s = s.strip()
        if len(s) == 0:
            raise ValueError("String is longer than one character.")
        return s
    raise ValueError('Single-character string is expected.')