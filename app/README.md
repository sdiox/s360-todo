# s360-todo
An implementation of to-do list backend RESTful API as an assignment from a Scale360 interviewer. It is developed in **Python 3.7** with Flask, Flask-RESTful, Flask-JWT-Extended, and

This to-do list implementation requires users to register an account and log in before interacting with the REST APIs provided by the implementation.


## Installation
1. Make sure the version of Python in your environment is 3.7 or higher by running:

        $ python --version
          Python 3.7.1

    If the version of Python is lower than 3.7, [install Python 3.7 using `pyenv`](https://github.com/pyenv/pyenv) is recommended.

    After installing pyenv, run the following commands to install Python 3.7.1 and use it globally:

        $ pyenv install 3.7.1
        $ pyenv global 3.7.1

    Before proceeding to the next step, make sure that `python --version` returns `Python 3.7.1`.

2. Install `virtualenv`:

        $ pip install virtualenv

3. Create a virtual environment at `./todo-venv` (for example)

        $ python -m venv todo-venv

4. Activate the virtual environment created earlier:

        $ source ./todo-venv/bin/activate

5. Clone the repository and install dependencies listed in `requirements.txt` which is included in this repository.

        (todo-venv) $ pip install -r requirements.txt

6. Run unit test in `unit_tests.py` to see whether the APIs are functioning as expected:

        (todo-venv) $ python unit_tests.py

7. Run `run.py` to start the REST API server for this to-do list implementation:

        (todo-venv) $ python run.py

8. Access the REST APIs with url `http://127.0.0.1:5000`


## Quick Start
Refer to API Documentation for any requests mentioned in the steps listed below:
1. Register a new account with request [`POST /registration`](#register-a-new-account) or login with an existing account with request [`POST /login`](#login-to-the-api)
2. Use `access_token` and `refresh_token` obtained from one of the requests from the previous step to proceed.
3. Add a new to-do list item with request [`POST /todo_item/add`](#add-a-new-item-into-users-to-do-list)
4. View all to-do list items belonging to this account with request [`GET /todo_items`](#get-to-do-list-items-created-by-a-username) or view a to-do list item with request [`GET /todo_item/:item_id`](#get-a-to-do-list-item-created-by-a-username)
5. Edit a to-do list item or change its status with request [`POST /todo_item/:item_id`](#edit-a-to-do-list-item-created-by-a-username) or [`PUT /todo_item/:item_id`](#edit-a-to-do-list-item-created-by-a-username)
6. Delete a to-do list item with request [`DELETE /todo_item/:item_id`](#delete-a-to-do-list-item-created-by-a-username)
7. Log out of API with request [`DELETE /logout/access`](#revoke-access-token) to revoke `access_token` and request [`DELETE /logout/refresh`](#revoke-refresh-token) to revoke `refresh_token`


## API Documentation

The parameters for each endpoint must be provided in JSON format.

## APIs Regarding Account Registration and Authorization

### Register a new account

Register an account to the API so that it can be used to retrieve access token and refresh token from the API for further authorization to the API. Note that `password` will be hashed with SHA-256 algorithm.

    POST /registration

##### HTTP Header
| Property     | Value            |
| :----------- | :--------------- |
| Content-type | application/json |

##### Parameters
| Name       | Type     | Description                | Example Value |
| :--------- | :------- | -------------------------- | :------------ |
| `username` | `string` | Username to register       | `test_user`   |
| `password` | `string` | Password for this username | `PassWord34#$`|

##### Response if username already exists
    Status: 400 Bad Request
    Response body:
    {
        "message": "User \"test_user\" already exists."
    }

##### Response for successful registration
    Status: 201 Created
    Response body:
    {
        "message": "User \"test_user\" has been created successfully.",
        "access_token": "...",
        "refresh_token": "..."
    }

----
### Login to the API

Logs in into the API. User must supply correct username and password in order to obtain access token and refresh token which is required for authorization to the other APIs.

    POST /login

##### HTTP Header
| Property     | Value            |
| :----------- | :--------------- |
| Content-type | application/json |

##### Parameters
| Name       | Type     | Description                | Example Value |
| :--------- | :------- | -------------------------- | :------------ |
| `username` | `string` | Username to register       | `test_user`   |
| `password` | `string` | Password for this username | `PassWord34#$`|

##### Response if wrong username or password are provided
    Status: 400 Bad Request
    Response body:
    {
        "message": "Wrong credentials."
    }

##### Response for successful login
    Status: 200 OK
    Response body:
    {
        "message": "Logged in as \"test_user\"",
        "access_token": "...",
        "refresh_token": "..."
    }

----
### Refresh access token

As access token expires in 30 minutes, a new access token can be requested by sending refresh token provided when logging into the API. This endpoint does not need any parameters.

    POST /token/refresh

##### HTTP Header
| Property      | Value                  |
| :------------ | :--------------------- |
| Content-type  | application/json       |
| Authorization | Bearer `refresh_token` |

##### Response when valid refresh token is provided
    Status: 200 OK
    Response body:
    {
        "access_token": "...",
    }

----
### Revoke access token

Revoke access token so that it cannot be used to access the APIs afterwards. This endpoint does not need any parameters.

    DELETE /logout/access

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Response when valid and *active* access token is provided
    Status: 200 OK
    Response body:
    {
        "message": "Successfully logged out"
    }

##### Response when a revoked access token is provided
    Status: 401 Unauthorized
    Response body:
    {
        "message": "Token has been revoked"
    }

----
### Revoke refresh token

Revoke refresh token so that it cannot be used to request a new access token. This endpoint does not need any parameters.

    DELETE /logout/refresh

##### HTTP Header
| Property      | Value                  |
| :------------ | :--------------------- |
| Content-type  | application/json       |
| Authorization | Bearer `refresh_token` |

##### Response when valid and *active* access token is provided
    Status: 200 OK
    Response body:
    {
        "message": "Successfully logged out"
    }

##### Response when a revoked access token is provided
    Status: 401 Unauthorized
    Response body:
    {
        "message": "Token has been revoked"
    }


## APIs Concerning Interactions with Items in To-do List

### Add a new item into user's to-do list

Add a new item into user's to-do list. All parameters except `detail` are required.

    POST /todo_item/add

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Parameters
| Name       | Type     | Description                |
| :--------- | :------- | -------------------------- |
| `subject`  | `string` | Required                   |
| `detail`   | `string` | Not required               |
| `status`   | `char`   | Must be either `P` or `D`  |

##### A Sample Response when one of the parameters are invalid
    Status: 400 Bad Request
    Response body:
    {
        "message": {
            "status": "status can be either P or D"
        }
    }

##### Response the provided parameters are valid
    Status: 200 OK
    Response body:
    {
        "id": 1,
        "subject": "test subject",
        "detail": "test detail",
        "status": "P"
    }

----
### Get to-do list items created by a username

Get all items in to-do list belonging to a username. Besides a valid access token, this endpoint does not need any parameters.

    GET /todo_items

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Response when valid access token is provided
    Status: 200 OK
    Response body:
    {
        "items": [{"id": 1, "subject": "test subject", "detail": "test detail", "status": "P"}]
    }

### Get a to-do list item created by a username

Get a to-do list item belonging to a username. This endpoint requires a valid access token.

    GET /todo_item/:item_id

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Response when an invalid item_id is provided
    Status: 400 Bad Request
    Response body:
    {
        "message": "Invalid item ID."
    }

##### Response when a valid item_id is provided
    Status: 200 OK
    Response body:
    {
        "item": {"id": 1, "subject": "test subject", "detail": "test detail", "status": "P"}
    }

----
### Edit a to-do list item created by a username

Edit subject, detail, and status of a to-do list item belonging to a username. This endpoint accepts both `POST` and `PUT` requests.

    POST /todo_item/:item_id
    PUT /todo_item/:item_id

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Parameters
| Name       | Type     | Description                |
| :--------- | :------- | -------------------------- |
| `subject`  | `string` | Required                   |
| `detail`   | `string` | Not required               |
| `status`   | `char`   | Must be either `P` or `D`  |

##### Response when an invalide item_id is provided
    Status: 400 Bad Request
    Response body:
    {
        "message": "Cannot find item with ID 1"
    }

##### Response when a valid item_id is provided along with valid parameters
    Status: 200 OK
    Response body:
    {
        "id": 1,
        "subject": "edited test subject",
        "detail": "edited test detail",
        "status": "D"
    }

----
### Delete a to-do list item created by a username

Delete a to-do list item belonging to a username. Besides a valid `item_id`, this endpoint does not need any parameters.

    DELETE /todo_item/:item_id

##### HTTP Header
| Property      | Value                 |
| :------------ | :-------------------- |
| Content-type  | application/json      |
| Authorization | Bearer `access_token` |

##### Response when an invalid item_id is provided
    Status: 400 Bad Request
    Response body:
    {
        "message": "Invalid item ID."
    }

##### Response when a valid item_id is provided
    Status: 200 OK
    Response body:
    {
        "message": "Item deleted."
    }
