from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager

import security
from security import SECRET_KEY
from datetime import timedelta

from resources.user import (UserLogin, UserRegistration, UserLogoutRefresh, UserLogoutAccess, TokenRefresh)
from resources.todo_list import TodoItemAccessor, TodoItemList, TodoItemInserter
from config import app_config

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(app_config[config_name])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.config['PROPAGATE_EXCEPTIONS'] = True
    app.config['JWT_SECRET_KEY'] = SECRET_KEY
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(seconds=1800)

    jwt = JWTManager(app)
    security.set_jwt_loaders(jwt)

    from database import db
    db.init_app(app)

    @app.before_first_request
    def create_tables():
        db.create_all()

    api = Api(app)

    # APIs regarding user registration and authorization
    api.add_resource(UserRegistration, '/registration')
    api.add_resource(UserLogin, '/login')
    api.add_resource(UserLogoutAccess, '/logout/access')
    api.add_resource(UserLogoutRefresh, '/logout/refresh')
    api.add_resource(TokenRefresh, '/token/refresh')

    api.add_resource(TodoItemAccessor, '/todo_item/<int:item_id>')
    api.add_resource(TodoItemInserter, '/todo_item/add')
    api.add_resource(TodoItemList, '/todo_items')

    return {'app': app, 'jwt': jwt, 'api': api}

