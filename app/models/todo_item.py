from database import db
from models.user import UserModel


class TodoItemModel(db.Model):
    __tablename__ = 'todo_items'

    ITEM_STATUS = ('P', 'D')

    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(200), nullable=False)
    detail = db.Column(db.String(500), nullable=True)
    # Status "P" for pending, and "D" for done
    status = db.Column(db.String(1), nullable=False, default="P")

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('UserModel')

    def __init__(self, user_id, subject, detail, status="P"):
        self.user_id = user_id
        self.subject = subject
        self.detail = detail
        self.status = status

    def json(self):
        return {'id': self.id,
                'subject': self.subject,
                'detail': self.detail,
                'status': self.status}

    @classmethod
    def find_by_user_id(cls, user_id):
        return cls.query.filter_by(user_id=user_id).all()

    @classmethod
    def find_by_username(cls, username):
        return db.session.query(cls).join(UserModel).filter(UserModel.username==username).all()

    @classmethod
    def find(cls, user_id, item_id):
        return cls.query.filter_by(user_id=user_id, id=item_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

