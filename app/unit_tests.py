import unittest
import json
import os
from app import create_app
from database import db
from models.user import UserModel

class UserResourceTestCase(unittest.TestCase):
    def setUp(self):
        self.flask_objects = create_app(config_name='testing')
        self.app = self.flask_objects['app']
        self.client = self.app.test_client
        self.test_account = {'username': 'test_user', 'password': 'PassWord34#$'}

        with self.app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def register_test_account(self):
        response = self.client().post('/registration', data=self.test_account)
        self.assertEqual(response.status_code, 201)

        return response

    def login_test_account(self):
        response = self.client().post('/login', data=self.test_account)
        self.assertEqual(response.status_code, 200)

        return response

    def test_account_registration(self):
        # Test account registration with valid parameters
        response = self.client().post('/registration', data=self.test_account)
        self.assertEqual(response.status_code, 201)

        response_json = json.loads(response.data)
        self.assertIn('created successfully', response_json['message'])
        self.assertTrue('access_token' in response_json)
        self.assertTrue('refresh_token' in response_json)

    def test_account_registration_with_blank_username(self):
        response = self.client().post('/registration', data={'username': '', 'password': 'PassWord34#$'})
        self.assertEqual(response.status_code, 400)
        response_json = json.loads(response.data)
        self.assertIn('not be blank', response_json['message']['username'])

    def test_account_registration_with_blank_password(self):
        response = self.client().post('/registration', data={'username': 'test_user', 'password': ''})
        self.assertEqual(response.status_code, 400)
        response_json = json.loads(response.data)
        self.assertIn('not be blank', response_json['message']['password'])

    def test_account_login(self):
        reg_response = self.register_test_account()
        login_response = self.login_test_account()

        login_response_json = json.loads(login_response.data)
        self.assertIn('Logged in as', login_response_json['message'])
        self.assertTrue('access_token' in login_response_json)
        self.assertTrue('refresh_token' in login_response_json)

    def test_invalid_account_login(self):
        login_response = self.client().post('/login', data={'username': 'wrong_user', 'password': 'wrong_password'})
        self.assertEqual(login_response.status_code, 400)
        login_response_json = json.loads(login_response.data)
        self.assertIn('Wrong credentials', login_response_json['message'])


    def test_access_token_refresh(self):
        reg_response = self.register_test_account()
        login_response = self.login_test_account()
        login_response_json = json.loads(login_response.data)
        self.assertTrue('access_token' in login_response_json)
        self.assertTrue('refresh_token' in login_response_json)

        access_token = login_response_json['access_token']
        refresh_token = login_response_json['refresh_token']

        headers = {'Authorization': f'Bearer {refresh_token}'}

        refresh_response = self.client().post('/token/refresh', headers=headers)
        self.assertEqual(refresh_response.status_code, 200)
        refresh_response_json = json.loads(refresh_response.data)
        self.assertTrue('access_token', refresh_response_json)
        self.assertNotEqual(access_token, refresh_response_json['access_token'])

    def test_invalid_access_token_refresh(self):
        refresh_response = self.client().post('/token/refresh', headers={'Authorization': 'Bearer invalid_refresh_token'})
        self.assertEqual(refresh_response.status_code, 401)
        refresh_response_json = json.loads(refresh_response.data)
        self.assertEqual(refresh_response_json['message'], 'Invalid token')

    def test_revoke_access_token(self):
        reg_response = self.register_test_account()
        login_response = self.login_test_account()

        login_response_json = json.loads(login_response.data)
        access_token = login_response_json['access_token']

        headers = {'Authorization': f'Bearer {access_token}'}
        revoke_access_res = self.client().delete('/logout/access', headers=headers)
        self.assertEqual(revoke_access_res.status_code, 200)
        revoke_access_res_json = json.loads(revoke_access_res.data)
        self.assertIn('logged out', revoke_access_res_json['message'])

        # try sending the same access token again
        revoke_access_res = self.client().delete('/logout/access', headers=headers)
        self.assertEqual(revoke_access_res.status_code, 401)
        revoke_access_res_json = json.loads(revoke_access_res.data)
        self.assertIn('been revoked', revoke_access_res_json['message'])

    def test_revoke_invalid_access_token(self):
        revoke_access_res = self.client().delete('/logout/access', headers={'Authorization': 'Bearer invalid_token'})
        self.assertEqual(revoke_access_res.status_code, 401)
        revoke_access_res_json = json.loads(revoke_access_res.data)
        self.assertEqual(revoke_access_res_json['message'], 'Invalid token')

    def test_revoke_refresh_token(self):
        reg_response = self.register_test_account()
        login_response = self.login_test_account()

        login_response_json = json.loads(login_response.data)
        refresh_token = login_response_json['refresh_token']

        headers = {'Authorization': f'Bearer {refresh_token}'}
        revoke_refresh_res = self.client().delete('/logout/refresh', headers=headers)
        self.assertEqual(revoke_refresh_res.status_code, 200)
        revoke_refresh_res_json = json.loads(revoke_refresh_res.data)
        self.assertIn('logged out', revoke_refresh_res_json['message'])

        # try sending the same access token again
        revoke_refresh_res = self.client().delete('/logout/refresh', headers=headers)
        self.assertEqual(revoke_refresh_res.status_code, 401)
        revoke_access_res_json = json.loads(revoke_refresh_res.data)
        self.assertIn('been revoked', revoke_access_res_json['message'])

    def test_revoke_invalid_refresh_token(self):
        revoke_refresh_res = self.client().delete('/logout/refresh', headers={'Authorization': 'Bearer invalid_token'})
        self.assertEqual(revoke_refresh_res.status_code, 401)
        revoke_refresh_res_json = json.loads(revoke_refresh_res.data)
        self.assertEqual(revoke_refresh_res_json['message'], 'Invalid token')

class TodoItemTestCase(unittest.TestCase):
    def setUp(self):
        self.flask_objects = create_app(config_name='testing')
        self.app = self.flask_objects['app']
        self.client = self.app.test_client
        self.test_account = {'username': 'test_user', 'password': 'PassWord34#$'}

        reg_response = self.register_test_account()
        login_response = self.login_test_account()
        login_response_json = json.loads(login_response.data)
        self.access_token = login_response_json['access_token']

        self.authorized_headers = {'Authorization': f'Bearer {self.access_token}'}

        with self.app.app_context():
            db.create_all()

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()

    def register_test_account(self):
        response = self.client().post('/registration', data=self.test_account)
        self.assertEqual(response.status_code, 201)

        return response

    def login_test_account(self):
        response = self.client().post('/login', data=self.test_account)
        self.assertEqual(response.status_code, 200)

        return response

    def add_test_item(self):
        data = {'subject': 'test_subject', 'detail': 'test_detail', 'status': 'P'}
        response = self.client().post('/todo_item/add', headers=self.authorized_headers, data=data)
        self.assertEqual(response.status_code, 201)

        return response

    def test_add_item(self):
        data = {'subject': 'test_subject', 'detail': 'test_detail', 'status': 'P'}
        response = self.client().post('/todo_item/add', headers=self.authorized_headers, data=data)
        self.assertEqual(response.status_code, 201)

        response_json = json.loads(response.data)
        self.assertEqual(response_json['subject'], data['subject'])
        self.assertEqual(response_json['detail'], data['detail'])
        self.assertEqual(response_json['status'], data['status'])

    def test_add_item_with_blank_subject(self):
        data = {'subject': '', 'detail': 'test_detail', 'status': 'P'}
        response = self.client().post('/todo_item/add', headers=self.authorized_headers, data=data)
        self.assertEqual(response.status_code, 400)

        response_json = json.loads(response.data)
        self.assertIn('cannot be blank', response_json['message']['subject'])

    def test_add_item_with_invalid_status(self):
        data = {'subject': 'test_subject', 'detail': 'test_detail', 'status': 'x'}
        response = self.client().post('/todo_item/add', headers=self.authorized_headers, data=data)
        self.assertEqual(response.status_code, 400)

        response_json = json.loads(response.data)
        self.assertIn('either P or D', response_json['message']['status'])

    def test_get_item(self):
        add_response = self.add_test_item()
        test_item_json = json.loads(add_response.data)

        response = self.client().get(f'/todo_item/{test_item_json["id"]}', headers=self.authorized_headers)
        self.assertEqual(response.status_code, 200)

        response_json = json.loads(response.data)
        self.assertEqual(response_json['item']['id'], test_item_json['id'])
        self.assertEqual(response_json['item']['subject'], test_item_json['subject'])
        self.assertEqual(response_json['item']['detail'], test_item_json['detail'])
        self.assertEqual(response_json['item']['status'], test_item_json['status'])

    def test_get_items(self):
        add_response = self.add_test_item()
        test_item_json = json.loads(add_response.data)

        response = self.client().get(f'/todo_items', headers=self.authorized_headers)
        self.assertEqual(response.status_code, 200)

        response_json = json.loads(response.data)
        self.assertEqual(len(response_json['items']), 1)
        self.assertEqual(response_json['items'][0]['subject'], test_item_json['subject'])
        self.assertEqual(response_json['items'][0]['detail'], test_item_json['detail'])
        self.assertEqual(response_json['items'][0]['status'], test_item_json['status'])

    def test_edit_item_with_post_request(self):
        add_response = self.add_test_item()
        test_item_json = json.loads(add_response.data)

        edited_item = test_item_json.copy()
        edited_item['subject'] = 'edited subject'
        edited_item['detail'] = 'edited detail'
        edited_item['status'] = 'D'

        response = self.client().post(f'/todo_item/{test_item_json["id"]}', headers=self.authorized_headers,
                                      data=edited_item)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['id'], edited_item['id'])
        self.assertEqual(response_json['subject'], edited_item['subject'])
        self.assertEqual(response_json['detail'], edited_item['detail'])
        self.assertEqual(response_json['status'], edited_item['status'])

    def test_edit_item_with_put_request(self):
        add_response = self.add_test_item()
        test_item_json = json.loads(add_response.data)

        edited_item = test_item_json.copy()
        edited_item['subject'] = 'edited subject'
        edited_item['detail'] = 'edited detail'
        edited_item['status'] = 'D'

        response = self.client().put(f'/todo_item/{test_item_json["id"]}', headers=self.authorized_headers,
                                      data=edited_item)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['id'], edited_item['id'])
        self.assertEqual(response_json['subject'], edited_item['subject'])
        self.assertEqual(response_json['detail'], edited_item['detail'])
        self.assertEqual(response_json['status'], edited_item['status'])

    def test_delete_item(self):
        add_response = self.add_test_item()
        test_item_json = json.loads(add_response.data)

        response = self.client().delete(f'/todo_item/{test_item_json["id"]}', headers=self.authorized_headers)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['message'], 'Item deleted.')

        response = self.client().get(f'/todo_item/{test_item_json["id"]}', headers=self.authorized_headers)
        self.assertEqual(response.status_code, 400)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['message'], 'Invalid item ID.')

    def test_delete_item_with_invalid_id(self):
        response = self.client().get(f'/todo_item/0', headers=self.authorized_headers)
        self.assertEqual(response.status_code, 400)
        response_json = json.loads(response.data)
        self.assertEqual(response_json['message'], 'Invalid item ID.')


if __name__ == '__main__':
    unittest.main()
